﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Pallindrome.aspx.cs" Inherits="BonusAssignment_BinitaPatel_n01283919.Pallindrome" %>
<asp:Content ID="Question" ContentPlaceHolderID="Content1" runat="server">
    <div> 
        <br />
        <br />
     Palindrome Checker... 
        <br />
        <br />
    </div>
</asp:Content>
<asp:Content ID="userInputControls" ContentPlaceHolderID="Content2" runat="server">
    <div>
        <asp:Label runat="server">Enter your string  </asp:Label>
        <asp:TextBox runat="server" ID="InputString" placeholder="Enter your string"></asp:TextBox>
        <asp:RequiredFieldValidator ID="InputStringRequired" runat="server" ControlToValidate="InputString" ErrorMessage="Value required."></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="InputStringRegularExpression" runat="server" ControlToValidate="InputString" ErrorMessage="Please enter a character." ValidationExpression="[a-zA-Z ]+$" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
        <br />
        <br />
        <asp:Button ID="Submit" Text="Check" runat="server" OnClick="Check_Palindrome"/>
    </div>
</asp:Content>
<asp:Content ID="resultArea" ContentPlaceHolderID="Content3" runat="server">
    <div id="result" runat="server"></div>
</asp:Content>
