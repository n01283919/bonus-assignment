﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment_BinitaPatel_n01283919
{
    public partial class PrimeNumber : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Check_PrimeNumber(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }

            int userInput = int.Parse(userNumber.Text);

            bool flag = false;

            for (int i = userInput - 1; i > 1; i--)
            {
                if (userInput % i == 0)
                {
                    flag = true;
                    break;
                }
            }

            if (flag == false)
            {
                result.InnerHtml = "Yayy!! \"" + userInput + "\" is a Prime Number";
            }
            else
            {
                result.InnerHtml = "I am sorry! \"" + userInput + "\" is not a Prime Number";
            }
        }

    

    }
}