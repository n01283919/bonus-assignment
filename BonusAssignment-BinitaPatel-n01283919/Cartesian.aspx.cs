﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment_BinitaPatel_n01283919
{
    public partial class Cartesian : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Check_Quadrant_Value(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }

            double xaxisValue = Convert.ToDouble(InputXAxis.Text);
            double yaxisValue = Convert.ToDouble(InputYAxis.Text);

            if (xaxisValue > 0 && yaxisValue > 0)
            {
                result.InnerHtml = "<strong>Output:</strong> (" + InputXAxis.Text + "," + InputYAxis.Text + ") lies in Quadrant 1";
            }
            else if (xaxisValue < 0 && yaxisValue > 0)
            {
                result.InnerHtml = "<strong>Output:</strong> (" + InputXAxis.Text + "," + InputYAxis.Text + ") lies in Quadrant 2";
            }
            else if (xaxisValue < 0 && yaxisValue < 0)
            {
                result.InnerHtml = "<strong>Output:</strong> (" + InputXAxis.Text + "," + InputYAxis.Text + ") lies in Quadrant 3";
            }
            else if (xaxisValue > 0 && yaxisValue < 0)
            {
                result.InnerHtml = "<strong>Output:</strong> (" + InputXAxis.Text + "," + InputYAxis.Text + ") lies in Quadrant 4";
            }
        }

        protected void InputXAxisCustomValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (Convert.ToDouble(InputXAxis.Text) == 0)
            {
                args.IsValid = false;
            }
        }
        protected void InputYAxisCustomValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (Convert.ToDouble(InputYAxis.Text) == 0)
            {
                args.IsValid = false;
            }
        }

    }
}

