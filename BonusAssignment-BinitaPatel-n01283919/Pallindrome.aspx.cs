﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment_BinitaPatel_n01283919
{
    public partial class Pallindrome : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            result.InnerHtml = "";
        }
        protected void Check_Palindrome(object sender, EventArgs e)
        {

            if (!Page.IsValid)
            {
                return;
            }

            string userInput = InputString.Text;

            string userInputInLowerCase = userInput.ToLower();
            string userInputWithoutSpaces = userInputInLowerCase.Replace(" ", string.Empty);
            string userInputInReverse = string.Empty;



            /* Reffered
            Tihanyi, B. (2012, March 20). Check if a string is a palindrome. Retrieved October 23, 2018, 
            from https://stackoverflow.com/questions/9790749/check-if-a-string-is-a-palindrome  */


            int length = userInputWithoutSpaces.Length;
            bool flag = false;
            for (int i = 0; i < length / 2; i++)
            {
                if (userInputWithoutSpaces[i] != userInputWithoutSpaces[length - i - 1])
                {
                    flag = true;
                    break;
                }
            }

            if (flag == false)
            {
                result.InnerHtml = "Yayy!  \"" + userInput + "\" is a Palindrome String";
            }
            else
            {
                result.InnerHtml = "I am sorry! \"" + userInput + "\" is not a Palindrome String";
            }
        }
    }
}
