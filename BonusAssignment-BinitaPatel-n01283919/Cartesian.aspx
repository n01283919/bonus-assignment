﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Cartesian.aspx.cs" Inherits="BonusAssignment_BinitaPatel_n01283919.Cartesian" %>
<asp:Content ID="Question" ContentPlaceHolderID="Content1" runat="server">
    <div>
        <br />
        <br />
        Given input of two integers, create a server click function which prints a message to a blank div
        element of which quadrant the co-ordinate would fall. Validate your inputs to assume that x and
        y are non-zero (for the sake of simplicity).
        <br />
        <br />
    </div>
</asp:Content>
<asp:Content ID="userInputControls" ContentPlaceHolderID="Content2" runat="server">
    <div>
        <asp:Label runat="server">Enter x-axis value: </asp:Label>
        <asp:TextBox runat="server" ID="InputXAxis"></asp:TextBox>
        <asp:RequiredFieldValidator ID="InputXAxisRequired" runat="server" ControlToValidate="InputXAxis" ErrorMessage="Value required." ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="InputXAxisRegularExpression" runat="server" ControlToValidate="InputXAxis" ErrorMessage="Please enter a valid number to proceed." ValidationExpression="[\-]?[0-9]{0,7}([\.][0-9])?$" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
        <asp:CustomValidator ID="InputXAxisCustomValidator" runat="server" ControlToValidate="InputXAxis" OnServerValidate="InputXAxisCustomValidator_ServerValidate" ErrorMessage="X-Axis value must be a non-zero number." ForeColor="Red" Display="Dynamic"></asp:CustomValidator>
        <br />
        <asp:Label runat="server">Enter y-axis value: </asp:Label>
        <asp:TextBox runat="server" ID="InputYAxis"></asp:TextBox>
        <asp:RequiredFieldValidator ID="InputYAxisRequired" runat="server" ControlToValidate="InputYAxis" ErrorMessage="Value required." ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="InputYAxisRegularExpression" runat="server" ControlToValidate="InputYAxis" ErrorMessage="Please enter a valid number to proceed." ValidationExpression="[\-]?[0-9]{0,7}([\.][0-9])?$" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
        <asp:CustomValidator ID="InputYAxisCustomValidator" runat="server" ControlToValidate="InputYAxis" OnServerValidate="InputYAxisCustomValidator_ServerValidate" ErrorMessage="Y-Axis value must be a non-zero number." ForeColor="Red" Display="Dynamic"></asp:CustomValidator>
        <br />
        <br />
        <asp:Button ID="submitUserInputNumber" Text="Check" runat="server" OnClick="Check_Quadrant_Value"/>
    </div>
</asp:Content>
<asp:Content ID="resultArea" ContentPlaceHolderID="Content3" runat="server">
    <div id="result" runat="server"></div>
</asp:Content>
