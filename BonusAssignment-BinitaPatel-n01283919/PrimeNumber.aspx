﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PrimeNumber.aspx.cs" Inherits="BonusAssignment_BinitaPatel_n01283919.PrimeNumber" %>
<asp:Content ID="Question" ContentPlaceHolderID="Content1" runat="server">
    <div>
        <br />
        <br />
        Given a positive integer input, write a server click function which prints a message to a blank div. The message will determine if a number is prime or not 
        <br />
        <br />
    </div>
</asp:Content>
<asp:Content ID="userControls" ContentPlaceHolderID="Content2" runat="server">
    <div>
        <asp:Label runat="server">Enter number to be checked for Prime Number. </asp:Label>
        <asp:TextBox runat="server" ID="userNumber"></asp:TextBox>
        <asp:RequiredFieldValidator ID="userNumberRequired" runat="server" ControlToValidate="userNumber" ErrorMessage="Value required." ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="userNumberRegularExpression" runat="server" ControlToValidate="userNumber" ErrorMessage="Please enter a valid number." ValidationExpression="[0-9]+$" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
        <br />
        <br />
        <asp:Button ID="submitNumber" Text="Check" runat="server" OnClick="Check_PrimeNumber"/>
    </div>
</asp:Content>
<asp:Content ID="resultplace" ContentPlaceHolderID="Content3" runat="server">
    <div id="result" runat="server"></div>
</asp:Content>
